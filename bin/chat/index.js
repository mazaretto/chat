module.exports = io => {

    var clients = []
    var messages = []

    io.on('connection', socket => {
        var currentUser;

        // auto emit
        io.sockets.emit('clients_online', clients)

        // авторизация клиента через ВК
        socket.on('auth_client', data => {
            socket.on('disconnect', () => {
                clients = clients.filter(cl => cl.id !== data.id)

                io.sockets.emit('clients_online', clients)
            })
        })
        // пришло новое сообщение
        socket.on('new_message', data => {
            messages.push(data)

            io.sockets.emit('messages', messages.reverse()[0])
        })

        // новый клиент
        socket.on('new_client', data => {
            var searchClient = clients.filter(cl => cl.id === data.id)

            if(searchClient.length === 0) {
                clients.push(data)
            }
            // оповестить всех клиентов
            io.sockets.emit('clients_online', clients)
        })
    })
}
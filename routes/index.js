var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');

var VK = require('./VK/index');
var cookie = require('cookie')

/**
 * @param header
 * @param cook
 * @returns {*}
 */
var getCookie = (header, cook) => {
  var cookObj = {}

  if (header !== undefined) {
    var objcook = header.split(';').map(item => {
      var i = item.split('=')

      return {
        [i[0].trim()]:i[1]
      }
    }).map((item, i)=> {
      Object.assign(cookObj, item)
    })

    return cookObj[cook]
  }

  return false
}

/* GET home page. */
router.get('/', function(req, res) {

  var isUserLogged = getCookie(req.headers['cookie'], 'VKauth')

  console.log(isUserLogged)

  if (isUserLogged) {

    VK.getUserInfo({
      access_token: isUserLogged,
      fields: 'uid,first_name,last_name,screen_name,photo_big'
    }).then(data => {
      const { id, first_name, last_name, screen_name, photo_big } = data.response[0]
      res.render('index', { isAuth: true, id, first_name, last_name, screen_name, photo_big, exitURI: VK.getExitUri() })
    })
  } else {
    res.render('index', {
      VKauth: VK.getAuthorizeLink()
    });
  }
});

/**
 * GET exit uri
 */
router.get('/exit', function (req, res) {
  var isUserAuth = getCookie(req.header('Cookie'), 'VKauth')

  if (isUserAuth) {
    res.clearCookie('VKauth')
    res.writeHead(302, {
      'Location':'/'
    })
    res.end()
  } else {
    res.send('hello')
  }
})

/**
 * Authorization with VK
 */
router.get('/vk-auth', function (req, res) {
  if(req.param('code')) {

    fetch(VK.getTokenUri(req.param('code'))).then(data => {
      return data.json()
    }).then(response => {
      if (response.access_token) {
        res.setHeader('Set-Cookie', 'VKauth=' + response.access_token)
        res.writeHead(302, {
          'Location':'/'
        })
        res.end()
      }
    })

  }
})

module.exports = router;

var fetch = require('node-fetch')

module.exports = {
    client_id: '6835357',
    secret_key: 'VFsEpCIHhXYOsNQwhmZn',
    service_key: '7afca3b97afca3b97afca3b94c7a94ef2477afc7afca3b926b19dc4dbfa04ea5c63345a',
    redirectUri: 'http://localhost:3000/vk-auth',
    apiURI: 'https://api.vk.com/method/',
    authURI: 'https://oauth.vk.com/',

    getAuthorizeLink () {
        return `${this.authURI}authorize?client_id=${this.client_id}&redirect_uri=${this.redirectUri}&response_type=code`
    },

    getUserInfo({ access_token, fields }) {
        return new Promise(resolve => {
            fetch(`${this.apiURI}users.get?access_token=${access_token}&fields=${fields}&v=5.52`).then(data => {
                return data.json()
            }).then(response => resolve(response))
        })
    },

    getExitUri () {
        return `/exit`
    },

    getTokenUri (code) {
        return `${this.authURI}access_token?client_id=${this.client_id}&redirect_uri=${this.redirectUri}&code=${code}&client_secret=${this.secret_key}`
    }
}
const getCookie = (cookie) => {
    const cookObj = {}

    document.cookie.split(';').map(item => {
        const i = item.split('=')
        cookObj[i[0]] = i[1]
    })

    return {
        result: cookObj[cookie],
        isset: !!(cookObj[cookie])
    }
}

const defaultTitle = document.title
const onDocumentJoin = () => {
    document.onvisibilitychange = () => {
        document.title = defaultTitle
    }
}

const App = {
    socket: null,

    playMsg () {
        var audio = document.createElement('audio')
            audio.src = 'msg.wav'
            audio.autoplay = true
        document.body.appendChild(audio)
        setTimeout(() => {
            let isAudioIsset = document.querySelector('audio')

            if(isAudioIsset) isAudioIsset.remove()
        }, 1500)
    },

    connectSocket () {
        this.socket = io()

        this.parseUser(() => {
            this.chatForm = document.getElementById('chat_form')
            this.messageContainer = document.querySelector('.chat_view')
            this.startRequests()
        })
    },

    parseUser (cb) {
        const getProfileElements = Object.assign([], document.querySelectorAll('div[data-profile]')).map(item => {
            const { profile } = item.dataset
            const splitProfile = profile.split('@')

            return {
                [splitProfile[0]]: encodeURIComponent(splitProfile[1])
            }
        })

        const userObj = {}
        getProfileElements.map(el => {
            Object.assign(userObj, el)
        })

        this.userInfo = userObj

        cb()
    },

    addMessage ({ message, id, info, image }) {
        const   messageView = document.createElement('div')
                messageView.classList.add('chat_view-message','bg-light','p-2')
                messageView.dataset.id = id
                messageView.innerHTML = `
                    <div class="row">
                        <div class="col-sm-3">
                            <img src="${decodeURIComponent(image)}" class="rounded-circle" width="70">
                        </div>
                        <div class="col-sm-9"><p><a href="https://vk.com/id${id}" target="_blank">${info}</a></p>${message}</div>
                    </div>
                `

        this.messageContainer.appendChild(messageView)
    },

    setMessage (data) {
        const { message, user } = data
        const { id, image, info } = user

        this.addMessage({ id, image, info: decodeURIComponent(info), message })
    },

    createNewMessageTitle (msg) {
        document.title = msg
    },

    startRequests () {
        this.socket.emit('new_client', this.userInfo)
        this.socket.emit('auth_client', this.userInfo)

        this.socket.on('clients_online', data => {
            online.innerText = 'Онлайн: ' + data.length
        })

        this.socket.on('messages', data => {
            this.playMsg()

            if(data.length === undefined) {
                this.setMessage(data)
            } else if (data.length > 0) {
                data.map(item => this.setMessage(item))
            }
            if(document.hidden) this.createNewMessageTitle('Новое  сообщение:  1')
        })

        this.chatForm.addEventListener('submit', e => {
            e.preventDefault()

            const { message } = e.target

            if(message.value != '') {
                this.socket.emit('new_message', {
                    user: this.userInfo,
                    message: message.value
                })

                setTimeout(() => {
                    try {
                        document.querySelector('audio').remove()
                    } catch(e) {}
                })
            }
        })
    },
    init () {
        if(getCookie('VKauth').isset) {
            this.connectSocket()
        }
    }
}

App.init()
onDocumentJoin()